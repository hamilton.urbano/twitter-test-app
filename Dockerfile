FROM ruby:2.4.0
RUN apt-get clean && apt-get update -qq && apt-get install -y build-essential sudo locales libpq-dev nodejs git mysql-client imagemagick libmagickcore-dev libmagickwand-dev libreadline6 libreadline6-dev graphviz less --no-install-recommends && rm -rf /var/lib/apt/lists/*

RUN mkdir /home/twitter_app_rails
WORKDIR /home/twitter_app_rails
ADD Gemfile /home/twitter_app_rails/Gemfile
ADD Gemfile.lock /home/twitter_app_rails/Gemfile.lock
RUN bundle install

# Use en_US.UTF-8 as our locale
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#Database config
ADD config/database.yml /home/twitter_app_rails/config/database.yml

ADD . /home/twitter_app_rails

#Expose the 3000 port
EXPOSE 3000
